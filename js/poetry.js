AE = {}
AE.config = {
    connections: {
        neutral: "rgba(255, 255, 255, 0.2)",
        selected: "#ae0"
    }
}

AE.initPoetry = function () {
    var container = d3.select("#poetry");
    d3.json("data/poetry.json", function (error, poetry) {
        AE.poetry = poetry;

        /* Poems */
        poems = container
            .selectAll('section')
            .data(poetry)
            .enter()
            .append('section')
            .classed('poem', true)
            .classed('author-i', (d, i) => d.author == "i")
            .classed('author-m', (d, i) => d.author == "m");

        poems
            .append('div')
            .classed('text', true)
            .attr("id", d => d.id)
            .html(d => d.poem);

        /* Links */
        poems
            .append('div')
            .classed('nexts', true)
            .selectAll('a.next')
            .data(d => d.next || 0)
            .enter()
            .append('a')
            .classed('next', true)
            .attr('href', d => "#" + d)
            .text((d, i) => i);

        /* Graph */
        var graph = d3.select("#graph");
        var nodes = graph.selectAll("circle")
            .data(poetry)
            .enter()
            //.append("g")
            .append("circle")
            .attr("r", 5);

        var connections = [];
        poetry.forEach(row => {
            if (row.next) {
                row.next.forEach(next => {
                    connections.push({source: row.id, target: next});
                });
            }
        });
        var links = graph.selectAll("line")
            .data(connections)
            .enter()
            .append("line")
            .attr("stroke", AE.config.connections.neutral);

        /* Forces */
        var sim = d3.forceSimulation()
            .force("link", d3.forceLink().id(row => row.id))
            .force("center", d3.forceCenter(500, 300))
            .alphaTarget(1)
            .stop();

        sim.nodes(poetry);
        sim.force("link")
            .links(connections)
            .strength((d) => d.active ? 0.2 : 0.8)
            .distance((d) => d.active ? 100 : 250);

        var ticked = function () {
            links
                .attr("x1", d => d.source.x)
                .attr("y1", d => d.source.y)
                .attr("x2", d => d.target.x)
                .attr("y2", d => d.target.y);

            nodes
                .attr("cx", d => d.x)
                .attr("cy", d => d.y);
        }
        sim.on("tick", ticked);

        var run_sim = function () {
            sim.tick();
            ticked();
            requestAnimationFrame(run_sim);
        }
        requestAnimationFrame(run_sim);

        /* Moving through links */
        poems.each(function () {
            var parent = d3.select(this);
            var source = parent.datum().id;
            parent.selectAll('a.next').on('click', function () {
                var next = d3.select(this).attr('href').slice(1);
                var link = links.filter((d) =>
                                        d.target.id == next &&
                                        d.source.id == source);
                link.attr("stroke", AE.config.connections.selected);
                var datum = link.datum();
                datum.active = true;
                sim.force("link").links(connections);
                sim.restart();
            });
        });
    });
}

AE.initCamera = function () {
    var target = document.getElementById('camera');
    navigator.mediaDevices
        .getUserMedia({video: true, audio: false})
        .then(function (stream) {
            target.srcObject = stream;
            target.play();
        });
}

AE.initCamera();
AE.initPoetry();
